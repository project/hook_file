
hook_file

This is an experimental backport of Drupal 7's file.inc "hook_file" API to
Drupal 6. Things work the same, except anywhere you would call a file_xyz
function, you would call hook_file_xyz. Also, anywhere you would implement
hook_file_xyz, you would instead implement hook_hook_file_xyz.

This would give modules the new functionality, and when ultimately upgrading,
developers would replace the functions by simply changing all occurences of
"hook_file" to "file".
